// ****************************************************************
// �������� �������������� ������ 
// Header
//
// ****************************************************************

#ifndef __GENERAL_M
#define __GENERAL_M
//

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "stm32f1xx_hal.h"


// ---------------------------------------------------------------------------------

// �������� ���������� ������ �����������
#define LED1_PWM_CONTROL TIM2->CCR1
#define LED2_PWM_CONTROL TIM2->CCR2
#define LED3_PWM_CONTROL TIM2->CCR3
					
// �������� ���������� PWM for RGB 
#define R_PWM_CONTROL TIM3->CCR1
#define G_PWM_CONTROL TIM3->CCR2
#define B_PWM_CONTROL TIM2->CCR4

// ����� ������
#define M_KEY_PORT GPIOB
#define M_KEY_LINE GPIO_PIN_0

// ����� �������
#define M_ZP_PORT GPIOC
#define M_ZP_LINE GPIO_PIN_13



// ���������� ������ (��� ������ ����)

// ������ 
#define KEY_KEY  55  


typedef enum {_R=0, _G=1, _B=2} _color;
		
struct U_IN_DATA
    {
      uint8_t fdatready:1;
      uint8_t tempUBuffer[10];
    };




	 

// ��������� �������

// -------------------------------------------
// ������ ���� ����� ���������� ������������
void START_AllPWM(void);
// -------------------------------------------

// ------------------------------------------------------------
// �������� ������� ������ ������
// uint8_tDivScanKey -  ��������� �������� ��� ������ ������ !!! �� ������ ���� == 0		
// ���������� �������� � ���������� ������� 
// �������������� �������� (DivScanKey) ������ ������������ ������� ������ 20 ��� � �������
void	main_keyfunc (uint8_t DivScanKey);
// ------------------------------------------------------------
// �������� �������� ������� ������	
//  ���������� ��������� �������� (0 - ������� �� ����; 1 -  ���� �������)
inline uint8_t testkey_on(void);	
	
// ------------------------------------------------------------	
// �������� �������� ���������� ������	
//  ���������� ��������� �������� (0 - ���� ��� ����� ������ ������; 1 - ��� ������ ��������)
 uint8_t testkey_off(void);
	
// ------------------------------------------------------------
// 1.4. ������� �������� ������� ������	
inline void clrkey_on(void);
	
// ------------------------------------------------------------
// ��������� �������� ������� ������	
inline void setkey_on(void);  
	
// ------------------------------------------------------------
// ������� �������� ���������� ������	
inline void clrkey_off(void);
  
// ------------------------------------------------------------
// ��������� �������� ���������� ������	
inline void setkey_off(void);

// ------------------------------------------------------------
// �������� ��� ��������� ������� ������
// ���������� ��� ��������� ������� ������
inline uint8_t getkey_code (void);
// ------------------------------------------------------------
// ��������� ��������� ������� �� ��������� ����� � �������� ��������  
//  1 �������  ����� �������� ������� � ������� ���������� ������� main_keyfunc
//	0 - ������	
	void bell_on(uint16_t ttim);
// ------------------------------------------------------------
// ������� ������ ������� ��������� ����������
// uint16_t bright - ������� � �������� �������� (0 - 255)
// uint8_t n       - ����� ����������
void setBrightLED(uint16_t bright, uint8_t n);
// ------------------------------------------------------------
// ������� ������ ������� ��� �������� ������� ����� ���� �������� ����������
// uint16_t bright - �������
// _color color    - ���� �� ������������
void setBrightCOLOR(uint16_t bright, _color color);
// -------------------------------------------------------------
// �������� ���� � ��������� HTML #FFFFFF
// uint8_t* color -  ������ � ����� �����
void setColorRGB(uint8_t* color);

// -------------------------------------------------------------
// *******************************************************************************
// ��� ������ USART
// -----------------------------
//*************************************************************************************
// UA.1.1. ������� ����������� ������� ����������� ��������� ������ � ������ ���������� ��������� ����� � ����� ���������
// __RXTOUT_DET = 5 mc
// rxbCounter
void rxDetTimer(void);
// -------------------------------------------------------------	
// U.1.2. �-�� ���������� ������� UART �� �����
void usart_start_RX (void);
// -------------------------------------------------------------	
// U.1.3. ������� �������������� �����  ���������� � �������� �� ������ ��� ������
// ������ ���������� � ���������� ������� � ���������� 1 �� 
void getDataUart(void);
// -------------------------------------------------------------	
// ========================================================
// �������� ����������
void main_route (void);

								 
#endif
// **************************************************
//
