// ****************************************************************
// �������� �������������� ������ 
// CODE
//
// ****************************************************************

#include "general_m.h"

// 5�� ���������� ��� ����������� ���������
#define __RXTOUT_DET 5


// ������� ��������:
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart1;

// ������ ���������� ����������
// 

static uint8_t C_Key;  // ��� ��������� ������� ������
static uint8_t Key_On; // 
static uint8_t Key_Off; // 		

static	uint16_t BellTime; // ������������ ����� � ��

// ��� USART
static uint16_t tmr_rx_detect ;   // ������ ����������� ��������� ������
static uint8_t rxbCounter;        // ������� �������� ����
static uint8_t rx_work_buff[128]; // ������� ����� ���������
static uint8_t tx_work_buff[16];  // ������� ����� �����������

struct U_IN_DATA uInData01; // ������ ��������� �� USART




// �������
// -------------------------------------------
// ������ ���� ����� ���������� ������������
void START_AllPWM(void)
{
  // �������� ��������� ��� �� ���� ������� �������
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1); // �������� ��������� ��� TIM3_CH1
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2); // �������� ��������� ��� TIM3_CH2

  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1); // �������� ��������� ��� TIM2_CH1,2,3,4
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
}
// -------------------------------------------

// ------------------------------------------------------------
// 1.1. �������� ������� ������ ������
// uint8_tDivScanKey -  ��������� �������� ��� ������ ������
		
void	main_keyfunc (uint8_t DivScanKey)
	{	
		
		//uint8_t i, j ;     // ������ ���������
		uint16_t temp8_1;  // ��������� �������� ���������� ���� ������
		
		static uint8_t KeyTimerDivisor = 0;          // ������ �������� ������ ������

		// ��������� �����		
		 {
		   // 1. �������� ������� ���������
       if (BellTime!=0) HAL_GPIO_WritePin(M_ZP_PORT, M_ZP_LINE, GPIO_PIN_SET);			 
		   
			 // 2. ��������� ������ ������� � ����������
			 if (BellTime !=0) 
			  {
				  if (--BellTime==0) HAL_GPIO_WritePin(M_ZP_PORT, M_ZP_LINE, GPIO_PIN_RESET);	// ���������� �����
				}		
		 } 		

    KeyTimerDivisor++; // ����������� ������� �������� ��� ������ ������		
		if(KeyTimerDivisor<DivScanKey) return; // ��������� ���� �� ���� ������ ����� ������
      else
		   {
		      KeyTimerDivisor = 0; // �������� ��������
		   } 

		 
		
		// 1 ����������� ������		

    // ������ �������� ����� 
    if(HAL_GPIO_ReadPin(M_KEY_PORT, M_KEY_LINE) == GPIO_PIN_RESET)   
       {
         // ������� ���
				 temp8_1=0; // �������� ����
			 }
			else
				{
					// ���� �������
					temp8_1= KEY_KEY; // ��� ������
				}
		 
			
		// 2. ��������� ������� ���������
		      if (temp8_1==0)
            {
             // �� ���� ������ �� ������ (���� ����������)
             Key_Off = 1 ; // ��������� ��������� 
             }
            else
            {              
             // ���� ������� �� ������             
              if (Key_Off==1)
               {  
                 if (Key_On!=1)
                 {
                  // ���������� ����� ��� ������ ��� �������  ��� ���� ����������
                  Key_On = 1;       // ���������� ��������� ������� ������
                  C_Key=temp8_1 ;   // �������� ��� ������� ������									 
                  
									bell_on(150);     // ������ �������� �������� ������� - ����������
									 
                  Key_Off = 0 ;     // �������� ������� ���������� ������
									 
                 }
               }                              
            }		
						
	}
// ------------------------------------------------------------




//  �������� �������� ������� ������	
//  ���������� ��������� �������� (0 - ������� �� ����; 1 -  ���� �������)
extern inline uint8_t testkey_on(void)
  {
	  return (Key_On);
	}
	
// ------------------------------------------------------------	
//  �������� �������� ���������� ������	
//  ���������� ��������� �������� (0 - ���� ��� ����� ������ ������; 1 - ��� ������ ��������)
 uint8_t testkey_off(void)
  {
	  return (Key_Off);
	}
// ------------------------------------------------------------
//  ������� �������� ������� ������	
extern inline void clrkey_on(void)
  {
	  Key_On=0;
	}
	
// ------------------------------------------------------------
//  ��������� �������� ������� ������	
 void setkey_on(void)
  {
	  Key_On=1;
	}
	
// ------------------------------------------------------------
//  ������� �������� ���������� ������	
 void clrkey_off(void)
  {
	  Key_Off=0;
	}
	
// ------------------------------------------------------------
// ��������� �������� ���������� ������	
 void setkey_off(void)
  {
	  Key_Off=1;
	}
// ------------------------------------------------------------
// �������� ��� ��������� ������� ������
 uint8_t getkey_code (void)	
  {
		return C_Key;
	}	
// ------------------------------------------------------------
// ��������� ��������� ������� �� ��������� ����� � �������� ��������  
//  1 �������  ����� �������� ������� � ������� ���������� ������� main_keyfunc
//	0 - ������	
	void bell_on(uint16_t ttim)
	{
	  BellTime = ttim;	 
	}
//
// ------------------------------------------------------------	


// ------------------------------------------------------------
// �������� CALLBAck ��� ������������� ���������
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
   if (htim == &htim4)
    {
			 // ����� ������ c �������� 20Hz = 1000/50
			 main_keyfunc (50);
		}

}
//

// ===============================================================
// ��� ��������� ������������
// ------------------------------------------------------------
// ������� ������ ������� ��������� ����������
// uint16_t bright - ������� � �������� �������� (0 - 255)
// uint8_t n       - ����� ���������� (1,2,3)
void setBrightLED(uint16_t bright, uint8_t n)
 {
    if(bright>255) bright=255; // �������� ������������ �������� �������
    if(n>3) return;  // ������ �� ������ ���� �� ���������� ����� ����������
    if (n==0) n=1;

    switch (n)
    {
      case 1:
      {
        LED1_PWM_CONTROL = bright;
      }
      break;
      case 2:
      {
        LED2_PWM_CONTROL = bright;
      }
      break;
      case 3:
      {
        LED3_PWM_CONTROL = bright;
      }
      break;
    }
 }

// ------------------------------------------------------------

// ������� ������ ������� ��� �������� ������� ����� ���� �������� ����������
// uint16_t bright - �������
// _color color    - ���� �� ������������
void setBrightCOLOR(uint16_t bright, _color color)
{
   if(bright>255) bright = 255; // �������� ������������ �������
   switch(color)
    {
      case _R:
      {
        R_PWM_CONTROL = bright;
      }
      break;
      case _G:
      {
        G_PWM_CONTROL = bright;
      }
      break;
      case _B:
      {
        B_PWM_CONTROL = bright;
      }
      break;
    }
}

// ------------------------------------------------------------
// ��������������� �-��
// ����������� ����� �� ���� �������� ������� hex � ����� ���� uint8_t

uint8_t hex2char(uint8_t * pp)
{
   uint8_t i;
   uint8_t a[2];
   
   uint8_t * ar = pp;

    for(i = 0; i<2; i++)
    { 
      if((*ar>= 'a')&&(*ar<='f')) a[i] = (*ar)-'a'+10;
       else if((*ar>= 'A')&&(*ar<='F')) a[i] = (*ar)-'A'+10;
        else if((*ar>='0')&&(*ar<='9')) a[i] = (*ar)-'0';
         else return(0); 
      pp++; // ��������� ���������
    }    
    return( a[0]*16+a[1] );
}


// ------------------------------------------------------------
// �������� ���� � ��������� HTML #FFFFFF
// 
// uint8_t* color -  ������ � ����� ����� ����� ����
void setColorRGB(uint8_t* color)
{ 
  if (strlen((const char*)color)!=6) return;
  // ���������� �������
  setBrightCOLOR( hex2char(color) ,_R);
  // ���������� �������
  setBrightCOLOR( hex2char( &color[2] ) ,_G);
  // ���������� �����
  setBrightCOLOR( hex2char( &color[4] ) ,_B);
}
// -------------------------------------------------------------
// -------------------------------------------------------------
// *******************************************************************************
// ��� ������ USART
// -----------------------------
//*************************************************************************************
// UA.1.1. ������� ����������� ������� ����������� ��������� ������ � ������ ���������� ��������� ����� � ����� ���������
// __RXTOUT_DET = 5 mc
// rxbCounter

void rxDetTimer(void)
 {   
	 
	 UART_HandleTypeDef *UartHandle;
	 UartHandle = &huart1;
	 
    uint32_t isrflags   = READ_REG(UartHandle->Instance->SR  );
	  uint32_t cr1its     = READ_REG(UartHandle->Instance->CR1);
 
    /* UART in mode Receiver ---------------------------------------------------*/
    if((((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET)))
    {
			tmr_rx_detect =  __RXTOUT_DET;  // ������ ������� ��� ����������� ��������� ������� ������� �� USART
			rxbCounter++  ; // ��������� ������� �������� ����
    }	
 }
// -----------------------------------------------------------------------------------
	
// U.1.2. �-�� ���������� ������� UART �� �����
void usart_start_RX (void)
 { 
  // �������� ����� UART � �������������� IT "������� ���� �� �����"
  HAL_UART_Receive_IT (&huart1, rx_work_buff, 128); 		 	  
 
 }
// ----------------------------------------------------------------------------------------------------

//*************************************************************************************
// U.1.3. ������� �������������� �����  ���������� � �������� �� ������ ��� ������
// ������ ���������� � ���������� ������� � ���������� 1 ��
 
void getDataUart(void) 
{
  //
		
	uint16_t i; 								// ������� ��������	
	
  // ***************************
  // ������ ����������� ������ ������� ��� USART0
  if (tmr_rx_detect != 0) // �������� ���� ������ �� � ����
  { 
    tmr_rx_detect--; // ���������
   if(tmr_rx_detect == 0)   // �������� ���������� �� ������� ������
	 {		 
		HAL_UART_AbortReceive_IT(&huart1); // ������� ���� �� ����� �� ����� ��������� ������			  		 
		 
	 // ���������� ���������� ������ �� ��������� �����
	 i = 0; // ������� � 0 
   while (rxbCounter != 0) 
    {                  
      if (i<10) uInData01.tempUBuffer[i] = rx_work_buff[i]; // �������� ���� �� ��������� ����� 							
      i++; // ����������� ������� �������� ����
		  (rxbCounter)--;
    }		 
		
    // ��������� �������� ������
    if( i == 7) // �������� ���������� ������ �������
    {
      // ��� ������ �������� ���� ���������
      uInData01.fdatready = 1;
    }
		// ����������� ������ �� ����� 
    HAL_UART_Receive_IT (&huart1, rx_work_buff, 128); 		
	 }	
	}
//		
}
// -------------------------------------------------------------
// ������� �������� ������ � UART ����� DMA
// uint8_t* data - ��������� �� ������ ��� ��������
// uint8_t datalen - ���������� ���� ��� ��������
void txData(uint8_t* data, uint8_t datalen )
 {
    uint8_t i;

    if (datalen>16) return;  // ����� ���� ������� ������� ������
    
    // ��������� ���������� ���� ��� ����� 
    for (i = 0; i< 5; i++)
     {
        if (huart1.gState != HAL_UART_STATE_BUSY_TX) break; 
        HAL_Delay(2);
     }
    if (i>=5) return; // �� ������������ ���� ���������� ������� ����� �����

    
    // ���������� ������ � ������� ����� �����������
    for (i=0; i< datalen; i++) 
    {
      tx_work_buff[i] = data[i];
    }
    // �������� ������ (5�� �������)
    for(i = 0; i<5; i++)
    {
     if( HAL_UART_Transmit_DMA(&huart1, (uint8_t*)tx_work_buff, datalen)== HAL_OK) return;	// ��������� � ��������� ���� ������� �������
     HAL_Delay(2);
    }
 }


// ========================================================
// �������� ����������
void main_route (void)
{
   uint8_t tempbuff[16]; // ������� ������
   uint8_t mkey;         // ���������� ������
  
   uint16_t temp16;

   // 1. ��������� ������� ������ 
   if (testkey_on())
    {
      // �������� ������� ������� �� ������
      clrkey_on();// ������� �������� ������� �� ������
      sprintf( (char*)tempbuff, "KEY_CLK\r\n"); // ����������� ������ ��� ��������
      txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
    }

  if(uInData01.fdatready ==1)
  {
    // ������������ ������ ���� ���� �� ������
    uInData01.fdatready = 0;// ����� ������� ����
    uInData01.tempUBuffer[7]=NULL; // �������� ���� � ����� ��� �������� ��������������
    sprintf( (char*)tempbuff, "OK\r\n"); // ����������� ������ ������������� ��� ��������

    // ����������� ��������
    if(uInData01.tempUBuffer[0]!='#')temp16 = atoi( (const char*)&(uInData01.tempUBuffer[4]));
    // �������� �������� ������
    mkey = uInData01.tempUBuffer[0]; 
    if(mkey == 'C') mkey = '#';

    switch (mkey)
    {
      case 'L':
      {
        if ((temp16<=255)&&((uInData01.tempUBuffer[1]&0x0f) <=3 ))
         {
            setBrightLED(temp16, (uInData01.tempUBuffer[1])&0x0f);            
            txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
         }
        break;
      }
      case 'R':
      {
        if (temp16<=255)
         {
            setBrightCOLOR(temp16, _R);
            txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
         }
        break;
      }
      case 'G':
      {
        if (temp16<=255)
         {
            setBrightCOLOR(temp16, _G);
            txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
         }
        break;
      }
      case 'B':
      {
        if (temp16<=255)
         {
            setBrightCOLOR(temp16, _B);
            txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
         }
        break;
      }
      case '#':
      {
        setColorRGB( &(uInData01.tempUBuffer[1]));
        txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
        break;
      }
      case 'Z':
      {
        bell_on(temp16*10);
        txData(tempbuff, strlen((const char*)tempbuff)) ;// ��������� ������ �� ��������
        break;        
      }
    }

  }
  
    


}
//




//************************************************************************************* 


//*************************************************************************************




// ------------------------------------------------------------


	
		
		
// *************************************************************************************************

 /************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
